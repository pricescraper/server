from flask_script import Manager

from pricescraper import app
from pricescraper.command import get_all_commands


if __name__ == "__main__":
    manager = Manager(app)
    commands = get_all_commands()
    for command_name, command in commands.items():
        manager.add_command(command_name, command)
    manager.run()
