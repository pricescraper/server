import pkgutil
import pricescraper.task


def register_all_tasks():
    package = pricescraper.task
    for importer, modname, ispkg in pkgutil.iter_modules(package.__path__):
        if ispkg:
            continue
        __import__(
            'pricescraper.task.{}'.format(modname), fromlist='dummy')
