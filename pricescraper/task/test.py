import logging

from celery import current_app

from pricescraper import app
from pricescraper.model import db
from pricescraper.model.product import Product
from pricescraper.model.task import TaskSet, Task
from pricescraper.scraper import Scraper
from pricescraper.scraper.exceptions import WebsiteScraperException


@current_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls test('hello') every 300 seconds.
    sender.add_periodic_task(10.0, scrap, name='Scrap all products')


@current_app.task(name='scrap')
def scrap():
    with app.app_context():
        products = Product.query.all()
        for product in products:
            has_task = TaskSet.query.filter_by(
                product_id=product.id, status=TaskSet.STATUS_PROGRESS).first()
            if not has_task:
                scrap_manager = Scraper()

                task_set = TaskSet(product_id=product.id)
                db.session.add(task_set)
                db.session.commit()

                for scraper in product.scrapers:
                    task = Task(
                        taskset_id=task_set.id, product_scraper_id=scraper.id)
                    db.session.add(task)
                    db.session.commit()

                    try:
                        result = scrap_manager.scrap(scraper)
                        logging.info('[%s][%s] %s' % (
                            scraper.name, scraper.website, result))
                        task.result = result
                        task.status = Task.STATUS_OK
                    except WebsiteScraperException:
                        task.status = Task.STATUS_ERROR
                    finally:
                        db.session.add(task)
                        db.session.commit()

                task_set.status = TaskSet.STATUS_OK
                db.session.add(task_set)
                db.session.commit()
                scrap_manager.quit()
