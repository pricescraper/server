from flask_script import Manager, prompt_bool

from pricescraper.model import db


manager = Manager(usage="Perform database operations")


@manager.command
def drop():
    """Drop database tables."""
    if prompt_bool("Are you sure you want to lose all your data"):
        db.drop_all()


@manager.command
def create():
    """Create database tables from sqlalchemy models."""
    db.create_all()


@manager.command
def recreate():
    """Recreate database tables (same as issuing 'drop' and then 'create')."""
    drop()
    create()
