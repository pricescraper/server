from flask_script import Manager, prompt_bool
from flask import json

from pricescraper.endpoint import api


manager = Manager(usage="Perform api operations")


@manager.command
def export_as_postman():
    """Export API endpoints for postman."""
    data = api.as_postman(urlvars=False, swagger=True)
    print(json.dumps(data))
