import pkgutil
import pricescraper.command


def get_all_commands():
    package = pricescraper.command
    commands = dict()
    for importer, modname, ispkg in pkgutil.iter_modules(package.__path__):
        if ispkg:
            continue
        commands[modname] = __import__(
            'pricescraper.command.{}'.format(modname),
            fromlist='dummy').manager

    return commands
