from flask_restplus import fields

from pricescraper.endpoint import api
from pricescraper.endpoint.serializer.product_scraper import (
    product_scraper_serializer)
from pricescraper.model import db
from pricescraper.model.product import Product


product_serializer = api.model('Product', {
   'id': fields.Integer(
       readOnly=True, description='The unique identifier of a product'),
   'name': fields.String(required=True, description='Product name'),
   'latest_results': fields.Raw(attribute=lambda x: x._get_latest_results())
})


def create_product(data):
    name = data.get('name')
    product_id = data.get('id')

    product = Product(name)
    if product_id:
        product.id = product_id

    db.session.add(product)
    db.session.commit()

    return product


def update_product(product_id, data):
    product = Product.query.filter(Product.id == product_id).one()
    product.name = data.get('name')

    db.session.add(product)
    db.session.commit()

    return product


def delete_product(product_id):
    product = Product.query.filter(Product.id == product_id).one()

    db.session.delete(product)
    db.session.commit()
