from flask_restplus import fields

from pricescraper.endpoint import api
from pricescraper.endpoint.exception import ItemNotFoundException
from pricescraper.model import db
from pricescraper.model.product import Product, ProductScraper


product_scraper_serializer = api.model('ProductScraper', {
    'id': fields.Integer(
        readOnly=True,
        description='The unique identifier of a product scraper'),
    'name': fields.String(required=True, description='Product  scraper name'),
    'website': fields.String(required=True, description='Website name'),
    'path': fields.String(required=True, description='Product path'),
    '_params': fields.String(description='Product parameters')
})


def create_scraper(data, product_id):
    name = data.pop('name')
    scraper_id = data.get('id')

    product = Product.query.get(product_id)
    if product is None:
        raise ItemNotFoundException("Product not found")

    scraper = ProductScraper(name=name, product=product, **data)
    if scraper_id:
        scraper.id = scraper_id

    db.session.add(scraper)
    db.session.commit()

    return scraper


def update_scraper(product_id, id, data):
    scraper = ProductScraper.query.filter(
        ProductScraper.product_id == product_id, ProductScraper.id == id).one()
    scraper.name = data.get('name')

    db.session.add(scraper)
    db.session.commit()

    return scraper


def delete_scraper(product_id, id):
    scraper = ProductScraper.query.filter(
        ProductScraper.product_id == product_id,
        ProductScraper.id == id).one()

    db.session.delete(scraper)
    db.session.commit()
