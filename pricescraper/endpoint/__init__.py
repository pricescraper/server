from flask_restplus import Api

from pricescraper.endpoint.namespace import get_all_namespaces


api = Api(version='1.0', title='Pricescraper API',
          description='A pricescraper API')


def register_all_api(app):
    for namespace in get_all_namespaces():
        api.add_namespace(namespace)
    api.init_app(app)
