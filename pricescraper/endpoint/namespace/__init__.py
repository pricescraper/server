import pkgutil

import pricescraper.endpoint.namespace


def get_all_namespaces():
    package = pricescraper.endpoint.namespace
    namespaces = []
    for importer, modname, ispkg in pkgutil.iter_modules(package.__path__):
        if ispkg:
            continue
        namespaces.append(__import__(
            'pricescraper.endpoint.namespace.{}'.format(modname), fromlist='dummy').ns)

    return namespaces
