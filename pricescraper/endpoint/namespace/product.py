from flask_restplus import Namespace, Resource

from pricescraper.endpoint import api
from pricescraper.endpoint.serializer.product import product_serializer,\
    create_product, update_product, delete_product
from pricescraper.endpoint.serializer.product_scraper import (
    product_scraper_serializer, create_scraper, update_scraper, delete_scraper
)
from pricescraper.model.product import Product, ProductScraper


ns = Namespace('products', description='Operations related to products')


@ns.route('/')
class ProductCollection(Resource):
    @ns.doc('list_products')
    @ns.marshal_list_with(product_serializer)
    def get(self):
        products = Product.query.all()
        return products

    @ns.doc('create_product')
    @ns.expect(product_serializer)
    @ns.marshal_with(product_serializer, code=201)
    def post(self):
        data = api.payload
        product = create_product(data)
        return product, 201


@ns.route('/<int:id>')
class ProductItem(Resource):
    @ns.doc('list_products')
    @ns.marshal_list_with(product_serializer)
    def get(self, id):
        return Product.query.filter(Product.id == id).one()

    @ns.doc('update_product')
    @ns.expect(product_serializer)
    @ns.marshal_with(product_serializer, code=201)
    def put(self, id):
        data = api.payload
        update_product(id, data)
        return None, 204

    @ns.doc('delete_product')
    @ns.response(204, 'Product deleted')
    def delete(self, id):
        delete_product(id)
        return None, 204


@ns.route('/<int:product_id>/scrapers')
class ProductScraperCollection(Resource):
    @ns.doc('list_product_scrapers')
    @ns.marshal_list_with(product_scraper_serializer)
    def get(self, product_id):
        scrapers = ProductScraper.query.filter(
            ProductScraper.product_id == product_id).all()
        return scrapers

    @ns.doc('create_product_scraper')
    @ns.expect(product_scraper_serializer)
    @ns.marshal_with(product_scraper_serializer, code=201)
    def post(self, product_id):
        data = api.payload
        scraper = create_scraper(data, product_id)
        return scraper, 201


@ns.route('/<int:product_id>/scrapers/<int:id>')
class ProductScraperItem(Resource):
    @ns.doc('update_product_scraper')
    @ns.expect(product_scraper_serializer)
    @ns.marshal_with(product_scraper_serializer, code=201)
    def put(self, product_id, id):
        data = api.payload
        update_scraper(product_id, id, data)
        return None, 204

    @ns.doc('delete_product_scraper')
    @ns.response(204, 'Product scraper deleted')
    def delete(self, product_id, id):
        delete_scraper(product_id, id)
        return None, 204
