from werkzeug.exceptions import BadRequest, NotFound


class EndpointException(BadRequest):
    pass


class ItemNotFoundException(NotFound):
    pass
