# -*- coding: utf8 -*-

import logging
import pkgutil

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

from pricescraper.scraper.exceptions import WebsiteScraperException
import pricescraper.scraper.website
from pricescraper.scraper.website import Website


class Scraper(object):
    def __init__(self):
        self.display = self._initdisplay()
        self.webdriver = self._initwebdriver()
        self.scrapers = self._getscrapers()

    def scrap(self, scraper):
        if scraper.website not in self.scrapers:
            raise WebsiteScraperException(
                'Scraper "%s" not found, skipping' % scraper.website)
        website_scraper = self.scrapers.get(scraper.website)(
            self.webdriver,
            {
                'product_id': scraper.path,
                'options': scraper.params if scraper.params else {}
            }
        )
        return website_scraper.scrap()

    def quit(self):
        logging.debug('Closing browser and destroying virtual display ...')
        self.webdriver.quit()
        self.display.stop()

    def _initdisplay(self):
        logging.debug('Creating virtual display to hide browser ...')
        display = Display(visible=0, size=(800, 600))
        display.start()
        return display

    def _initwebdriver(self):
        logging.debug('Loading chrome driver ...')
        driver = webdriver.Chrome()
        driver.wait = WebDriverWait(driver, 5)
        return driver

    def _getscrapers(self):
        scrapers = dict()
        package = pricescraper.scraper.website
        for importer, modname, ispkg in pkgutil.iter_modules(package.__path__):
            if ispkg:
                continue
            logging.debug('Loading scraper "%s" ...' % modname)

            module = __import__(
                'pricescraper.scraper.website.{}'.format(modname),
                fromlist='dummy')
            for attr in dir(module):
                try:
                    if issubclass(getattr(module, attr), Website):
                        scraper = getattr(module, attr)
                        if scraper.NAME is not None:
                            scrapers[scraper.NAME] = scraper
                except TypeError:
                    pass
        return scrapers
