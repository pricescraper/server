# -*- coding: utf8 -*-
from urllib.parse import urljoin

from jsonschema import validate
from jsonschema.exceptions import ValidationError
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

from pricescraper.scraper.exceptions import WebsiteScraperException


class Website(object):
    NAME = None
    WEBSITE_URI = None
    SCRAP_BY_ID = None
    PRICE_STRIP = None
    CURRENCY = None
    PARAMS_SCHEMA = {
        "type": "object",
        "required": ["product_id"],
        "properties": {
            "product_id": {"type": "string"},
            "options": {
                "type": "object",
                "uniqueItems": True
            }
        },
        "additionalProperties": False
    }

    def __init__(self, driver, params={}):
        self._driver = driver
        self.params = params if self.validate_params(params) else {}

    def validate_params(self, params={}):
        try:
            validate(params, self.PARAMS_SCHEMA)
            return True
        except ValidationError as e:
            raise WebsiteScraperException(
                'Error while validating params : %s' % e.message)

    def get_product_uri(self):
        if not self.WEBSITE_URI:
            raise NotImplementedError()
        return urljoin(self.WEBSITE_URI, self.params.get('product_id'))

    def parse_price(self, price):
        price = price.strip(self.PRICE_STRIP)
        return price

    def scrap(self):
        if not self.SCRAP_BY_ID:
            raise NotImplementedError()

        try:
            self._driver.get(self.get_product_uri())
            self._driver.wait.until(
                EC.presence_of_element_located((By.ID, self.SCRAP_BY_ID)))
        except TimeoutException as e:
            raise WebsiteScraperException(
                'Unable to scrap %s : %s' % (self.get_product_uri(), e))

        return self.parse_price(self._driver.find_element_by_id(
            self.SCRAP_BY_ID).text)
