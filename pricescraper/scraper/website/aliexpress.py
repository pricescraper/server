# -*- coding: utf8 -*-
from pricescraper.scraper.website import Website


class AliexpressWebsite(Website):
    NAME = 'aliexpress'
    WEBSITE_URI = 'http://www.aliexpress.com/'
    CURRENCY = 'USD'
    PRICE_STRIP = 'US $'

    def scrap(self):
        self._driver.get(self.get_product_uri())
        for key, value in self.params.get('options').items():
            option_path = "//dt[contains(text(), '%s')]" % key
            option_value_path = (
                "%s/following-sibling::dd/ul/li/a"
                "[@title='%s' or contains(span, '%s')]" % (
                    option_path, value, value)
            )
            self._driver.wait.until(
                lambda d: d.find_elements_by_xpath(option_path))
            element = self._driver.find_element_by_xpath(option_value_path)
            element.click()

        return self.parse_price(
            self._driver.find_element_by_id('j-total-price-value').text)
