# -*- coding: utf8 -*-

from pricescraper.scraper.website import Website


class GearbestWebsite(Website):
    NAME = 'gearbest'
    WEBSITE_URI = 'http://www.gearbest.com/'
    SCRAP_BY_ID = 'unit_price'
    CURRENCY = 'EUR'
    PRICE_STRIP = '€'
