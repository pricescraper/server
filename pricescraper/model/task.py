from pricescraper.model import db, _get_date


class TaskSet(db.Model):
    __tablename__ = 'task_set'

    STATUS_ERROR = "ERROR"
    STATUS_OK = "OK"
    STATUS_PROGRESS = "PROGRESS"

    id = db.Column(db.Integer, primary_key=True)
    tasks = db.relationship(
        "Task", backref="taskset", cascade="all")
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'),
                           nullable=False)
    status = db.Column(
        db.Enum(
            STATUS_PROGRESS, STATUS_OK, STATUS_ERROR),
        nullable=False, default=STATUS_PROGRESS)
    created_at = db.Column(db.DateTime, default=_get_date)
    updated_at = db.Column(db.DateTime, onupdate=_get_date)


class Task(db.Model):
    __tablename__ = 'task'

    STATUS_ERROR = "ERROR"
    STATUS_OK = "OK"
    STATUS_PROGRESS = "PROGRESS"

    id = db.Column(db.Integer, primary_key=True)
    taskset_id = db.Column(db.Integer, db.ForeignKey('task_set.id'),
                           nullable=False)
    status = db.Column(
        db.Enum(
            STATUS_PROGRESS, STATUS_OK, STATUS_ERROR),
        nullable=False, default=STATUS_PROGRESS)
    product_scraper_id = db.Column(
        db.Integer, db.ForeignKey('product_scraper.id'), nullable=False)
    result = db.Column(db.String(255))
    created_at = db.Column(db.DateTime, default=_get_date)
    updated_at = db.Column(db.DateTime, onupdate=_get_date)
