import datetime

from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


def _get_date():
    return datetime.datetime.now()
