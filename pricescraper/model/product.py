import json

from sqlalchemy.schema import UniqueConstraint

from pricescraper.model import db
from pricescraper.model.task import TaskSet, Task


class Product(db.Model):
    __tablename__ = 'product'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    scrapers = db.relationship(
        "ProductScraper", backref="product", cascade="all")
    tasks = db.relationship(
        "TaskSet", backref="product", cascade="all")

    def _get_last_task(self):
        last_task = None
        for task in self.tasks:
            if task.status != TaskSet.STATUS_OK:
                continue

            if last_task is None or task.created_at > last_task.created_at:
                last_task = task
                continue

        return last_task

    def _get_latest_results(self):
        last_task = self._get_last_task()
        latest_result = {}
        if last_task:
            for task in last_task.tasks:
                if task.status == Task.STATUS_OK:
                    product = ProductScraper.query.filter(
                        ProductScraper.id == task.product_scraper_id).one()
                    latest_result[product.name] = {
                        'website': product.website,
                        'params': product.params,
                        'result': task.result
                    }
        return latest_result

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Product %r>' % self.name


class ProductScraper(db.Model):
    __tablename__ = 'product_scraper'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'),
                           nullable=False)
    website = db.Column(db.String(255), nullable=False)
    path = db.Column(db.String(255), nullable=False)
    _params = db.Column(db.Text())

    __table_args__ = (
        UniqueConstraint(
            'product_id', 'name', name='_product_name_uc'),
    )

    def __repr__(self):
        return '<ProductScraper %r>' % self.name

    @property
    def params(self):
        return json.loads(self._params)

    @params.setter
    def params(self, params):
        self._params = json.dumps(params)
