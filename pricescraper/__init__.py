from celery import Celery
from flask import Flask
from flask_restplus import Resource

from pricescraper.endpoint import api, register_all_api
from pricescraper.endpoint.namespace import get_all_namespaces
from pricescraper.model import db
from pricescraper.task import register_all_tasks

app = Flask(__name__)

# Init database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/pricescraper.db'
db.init_app(app)

# Register api endpoints
register_all_api(app)

# Init queue
app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379'
app.config['CELERY_DEFAULT_QUEUE'] = 'pricescraper_tasks'
queue = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
queue.conf.update(app.config)

# Register celery tasks
register_all_tasks()
