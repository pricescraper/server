Pricescraper
============

A simple tool to regularly check the price of some product on e-commerce website and keep an history of the fluctuation. This is intended to make some graph and notification about drop in prices.

This project is an early experimentation and has been built as a 24 hours personnal challenge and is not fully ready to be used. Right now, 13 of the expected 24 hours were used, the final goal is to see how far the project can be enhanced until the deadline.


Improvements
------------

This project was designed for experiment and to be made in 24hours, it lake in functionality have a lot of bugs and is in some parts really dirty. Here is an uncomplete list of improvements that can be made :
- Improve the price and currency management
- Using chromium headless instead of phantomjs
- Add a dockerfile to run the server
- Improve (or create ...) a proper documentation
- Add a notification system (mail, push ...) when prices decrease
- Improve code quality by using pylint/flake8 and by writing docstrings
- Add some unit tests
- Use celery cords and globally improve the task management
- Improve the database schema
- ...


Installation from sources
-------------------------

Create a [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/) and install python dependencies :
```
virtualenv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

You had to initialize the database by running the following command :
```
python run.py database create
```

After that you need to run the three main services :
- A redis server
- A celery worker
- The webservice that provide a rest api

### Redis server

The redis server can be installed on the host by following the [official installation guide]() or by running a server in a docker container, the second method is the preferred one :

```
docker run --name redis-server -p 6379:6379 -d redis:alpine
```

### A celery worker

Celery worker can be run using the following command :

```
celery -A pricescraper.queue -l debug worker -Q pricescraper_tasks --beat
```

### The webservice

Start the flask server :
```
python run.py runserver
```

### Poplating data
To start experimenting, you can populate some data with the REST API, for example to monitor the price of a Xiaomi Vacuum Robot on Gearbest you can do as follow :
```
# Insert product
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"name":"Xiaomi Vacuum Robot 2"}' 'http://localhost:5000/products/'

# Insert product scraper
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"name": "Gearbest", "website": "gearbest", "path": "robot-vacuum/pp_440546.html", "params": {}}' 'http://localhost:5000/products/1/scrapers'
```

If you prefere to have a UI, you must go on `http://localhost:5000` with your browser.
